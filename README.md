# React with ECMAScript 6

ebay example: React with ECMAScript 6.

## Blog
http://www.davidsellsconsulting.com/content/ecmascript-6-and-react-js/

## Usage

```zsh
npm install
npm run build
```

Optionally, to run from a web server:

```zsh
npm install -g live-server
live-server
webpack-dev-server --progress --colors

```
 python -m SimpleHTTPServer
```

An associated blog post can be found [here](http://www.jayway.com/2015/03/04/using-react-with-ecmascript-6/).

The gist of it is this:

* Using [webpack](http://webpack.github.io/), traverse the dependency tree.
* With the help of [babel](https://babeljs.io/), transpile any occurences of ECMAScript 6 syntax.
* Output the result to `dist/bundle.js`.

