/* jshint globalstrict: true, esnext: true */

'use strict';

import React from 'react';
import EbayService from './EbayService';
import StateObject from './StateObject';

/**
 * Using React 0.13.2
 * Updated 2015-04-28
 */


var stateObject = new StateObject;

// Thanks:http://www.newmediacampaigns.com/blog/refactoring-react-components-to-es6-classes
class BaseComponent extends React.Component {
  _bind(...methods) {
    methods.forEach((method) => this[method] = this[method].bind(this));
  }
}


class EbaySelect extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {selected: -1};
    this._bind('changeHandler');
    if (this.props.selectType !== 'category') {
      this._bind('applyStateChange');
      stateObject.subscribe('changeCategory', this.applyStateChange);
    }
  }

  applyStateChange() {
    this.props.options = stateObject.getOptions(this.props.selectType);
  }

  changeHandler(e) {
    this.state = {selected: e.target.value};
    stateObject.setSelectedAndNotify(this.props.selectType, this.state.selected);
    this.props.itemSelected(this.state.selected, this.props.selectType); // for update
  }


  getOptions() {
    return this.props.options.map(function (option, index) {
      return (
        <option key={index} value={option}>{option}</option>
      );
    });
  }

  render() {
    return (
      <select className="navigation" onChange={this.changeHandler} value={this.state.selected}>
        {this.getOptions()}
      </select>
    );
  }
}

class NavigationItem extends BaseComponent {
  constructor(props) {
    super(props);
    this._bind('onClick');
  }

  onClick() {
    this.props.itemSelected(this.props.item, this.props.selectType);
  }

  render() {
    return (
      <li onClick={this.onClick} className={this.props.selected ? "selected" : ""}>
        {this.props.item.value}
      </li>

    )
      ;
  }
}

class Navigation extends BaseComponent {
  constructor(props) {
    super(props);
    this._bind('applyStateChange', 'setItemSelected', 'setCategorySelected');
    this.state = {items: stateObject.getOptions('selection')};
  }

  componentDidMount() {
    stateObject.subscribe('categoryChange', this.applyStateChange);
  }

  applyStateChange() {
    this.setState({items: stateObject.getOptions('selection')});
    this.forceUpdate();
  }

  setItemSelected(item, selectType) {
    this.props.itemSelected(item, selectType);
  }

  setCategorySelected(item) {
  }

  render() {
    var _this = this;
    var items = this.state.items.map(function (value, index) {
      var item = {id: index, value: value};
      return (
        <NavigationItem key={item.id}
                        selectType={_this.props.selectType}
                        item={item}
                        itemSelected={_this.setItemSelected}
                        selected={item.id === _this.props.selectedSection}
          />
      );
    });


    return (
      <div className="navigation floating-menu">
        <div className="header">Navigation</div>
        <ul>
          {items}
        </ul>
        <div className="header">Brand</div>
        <EbaySelect selectType='brand' options={stateObject.getOptions('brand')}
                    itemSelected={_this.setItemSelected}></EbaySelect>

        <div className="header">Category</div>
        <EbaySelect selectType='category' options={stateObject.getOptions('category')}
                    itemSelected={_this.setCategorySelected}></EbaySelect>
      </div>
    );
  }
}

class ProductItem extends BaseComponent {
  constructor(props) {
    super(props);
  }

  static formatCurrency(val) {
    if (val) {
      var length = val.length;
      var dot = val.indexOf('.');
      if ((length - dot) < 3) {
        val = val + '0';
      }
    }
    return val;
  }

  static onClick(e) {
    window.open(e.adURL);
  }


  render() {
    var self = this;
    var item = this.props.item;
    return (
      <div className="card" onClick={ProductItem.onClick.bind(self,item)}>
        <div className="card_info">
          <div className="card_name"><p className="price">{item.price}</p></div>
          <div className="card_address">
            <div>{item.title[0]}</div>
          </div>
        </div>

        <img className="card_img" src={item.view}></img>
      </div>
    );
  }

}

class ProductList extends React.Component {
  constructor(props) {
    super(props);
  }


  static formatCurrency(val) {
    if (val) {
      var length = val.length;
      var dot = val.indexOf('.');
      if ((length - dot) < 3) {
        val = val + '0';
      }
    }
    return val;
  }

  static onClick(e) {
    window.open(e.adURL);
  }

  render() {
    var productNodes = this.props.items.map(function (item) {
      return (
        <ProductItem item={item}/>
      );
    });
    return (
      <div>
        {productNodes}
      </div>
    );
  }
}


class EbayApp extends BaseComponent {
  constructor(props) {
    super(props);
    this._bind('setSelectedItem','applyStateChange');
    this.state =
    {
      navigationItems: stateObject.getOptions('selection'),
      selectType: 'selection',
      productItems: [],
      title: "Please select a section",
      selectedSection: '',
      category: stateObject.getSelected('category')
    };
  }


  componentDidMount() {
    stateObject.subscribe('categoryChange', this.applyStateChange);

    var _this = this;
    var cbname = "fn" + Date.now();
    var script = document.createElement("script");

    window[cbname] = function () {
      _this.state.navigationItems = this.state.navigationItems;
      delete window[cbname];
    };

    document.head.appendChild(script);
  }

  applyStateChange() {
    // clear product list and de-select items
    this.setState({
      navigationItems: stateObject.getOptions('selection'),
      selectType: 'selection',
      productItems: [],
      title: "Please select a section",
      selectedSection: '',
      category: stateObject.getSelected('category')
    });
  }


  render() {
    return (
      <div>
        <h1 className='floating-title'>{this.state.title}</h1>
        <Navigation selectedSection={this.state.selectedSection}
                    selectType={this.state.selectType}
                    itemSelected={this.setSelectedItem}/>
        <ProductList items={this.state.productItems}/>
      </div>
    )
  }

  setSelectedItem(item, type) {
    if (type === 'selection') {
      stateObject.setSelected(type, item.value);
    } else {
      stateObject.setSelected(type, item);
    }
    var that = this;
    var title = stateObject.getTitle();

    EbayService.getInfoPromise(stateObject).then(function (jsonData) {
      that.state.productItems = jsonData;
      console.log('--------------------------- json data ----------------------');
      console.dir(jsonData);
      that.state.title = title;
      that.state.selectedSection = type == 'selection' ? item.id : that.state.selectedSection;
      that.forceUpdate();
    }, function (err) {
      alert('error: ' + err);
    });
  }
}


export default EbayApp;

