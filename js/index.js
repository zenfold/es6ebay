/* jshint globalstrict: true, esnext: true, browser: true */

'use strict';

import React from 'react';
import Ebay from './ebay';

React.render(
  <Ebay />,
  document.getElementById('body')
);

