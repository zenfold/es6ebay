/**
 * Created by david on 14/06/15.
 */

/**
 * Created by david on 14/06/15.
 */

import jQuery from 'jquery';


export default (function ($) {
    "use strict";
    var serviceObject = {};
    serviceObject.getInfoPromise = function (stateObject) {
        console.log('State Object in Service');
        console.dir(stateObject);
        // reject the promise
        var defer = new Promise(
            function (resolve, reject) {
                try {
                    var info = [];
                    var urlfilter = "";
                    // Create a JavaScript array of the item filters you want to use in your request
                    var filterarray = [
                        {
                            "name": "MaxPrice",
                            "value": "5000",
                            "paramName": "Currency",
                            "paramValue": "USD"
                        },
                        {
                            "name": "FreeShippingOnly",
                            "value": "false",
                            "paramName": "",
                            "paramValue": ""
                        },
                        {
                            "name": "ListingType",
                            "value": ["AuctionWithBIN", "FixedPrice", "StoreInventory"],
                            "paramName": "",
                            "paramValue": ""
                        },
                    ];

                    // Generates an indexed URL snippet from the array of item filters
                    var buildURLArray = function() {
                        // Iterate through each filter in the array
                        for (var i = 0; i < filterarray.length; i++) {
                            //Index each item filter in filterarray
                            var itemfilter = filterarray[i];
                            // Iterate through each parameter in each item filter
                            for (var index in itemfilter) {
                                // Check to see if the paramter has a value (some don't)
                                if (itemfilter[index] !== "") {
                                    if (itemfilter[index] instanceof Array) {
                                        for (var r = 0; r < itemfilter[index].length; r++) {
                                            var value = itemfilter[index][r];
                                            urlfilter += "&itemFilter\(" + i + "\)." + index + "\(" + r + "\)=" + value;
                                        }
                                    }
                                    else {
                                        urlfilter += "&itemFilter\(" + i + "\)." + index + "=" + itemfilter[index];
                                    }
                                }
                            }
                        }
                    }  // End buildURLArray() function

                    // Execute the function to build the URL filter
                    buildURLArray(filterarray);

                    var values = ' ';

                    if (stateObject.getSelected('brand') && stateObject.getSelected('brand').trim()) {
                        values += stateObject.getSelected('brand').trim() + ' ';
                    }
                    if (stateObject.getSelected('selection').trim()) {
                        values += stateObject.getSelected('selection').trim();
                    }
                    // Construct the request
                    // Replace MyAppID with your Production AppID
                    var url = "http://svcs.ebay.com/services/search/FindingService/v1";
                    url += "?OPERATION-NAME=findItemsByKeywords";
                    url += "&SERVICE-VERSION=1.0.0";
                    url += "&SECURITY-APPNAME=<EBAY CODE>";
                    url += "&GLOBAL-ID=EBAY-US";
                    url += "&RESPONSE-DATA-FORMAT=JSON";
                    url += "&callback=?";
                    url += "&REST-PAYLOAD";
                    url += "&keywords=" + values;
                    url += "&paginationInput.entriesPerPage=9";
                    url += urlfilter;

                    $.getJSON(url, {format: "json"})
                        .done(function (data) {
                            var items = data.findItemsByKeywordsResponse;

                            var products = data.findItemsByKeywordsResponse[0].searchResult[0].item;
                            info = [];
                            if (products) {
                                for (var count2 = 0; count2 < products.length; count2++) {
                                    var prod = products[count2];
                                    info.push({
                                        itemId: prod.itemId[0],
                                        adURL: prod.viewItemURL[0],
                                        title: prod.title,
                                        view: prod.galleryURL ? prod.galleryURL[0] : '',
                                        price: prod.sellingStatus[0].currentPrice[0].__value__
                                    });
                                }
                            }
                            resolve(info);
                        }).fail(function (err) {
                            console.log('error has occurred in EbayService: ' + err);
                            reject(err);
                        });
                } catch(err) {
                    console.log('Unexpected exception occurred in EbayService: ' + err);
                   reject(err);
                }
            }
        );
        return defer;
    };

    return serviceObject;
})
(jQuery);

