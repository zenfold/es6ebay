/**
 * Created by david on 28/06/15.
 */
class StateObject {
  constructor() {
    this.categories = ['audio', 'appliance', 'bikes'];

    this.categoryMap = {};

    // Audio
    this.audioBrands = ['Arcam', 'Cambridge', 'Linn', 'NAD', 'Bang and Olufsen', 'Samsung', 'Teac'];
    this.audioSelections = ['Amplifier', 'Speakers', 'CD Player', 'Receiver', 'Preamplifier'];

    this.audioOptions = {brand: this.audioBrands, category: this.categories, selection: this.audioSelections};
    this.audioSelected = {
      brand: this.audioBrands[0],
      selection: this.audioSelections[0],
      category: this.categories[0]
    };


    // Appliance
    this.applianceBrands = ['Maytag', 'Whirlpool', 'GE', 'Kenmore'];
    this.applianceSelections = ['Range', 'Freezer', 'Refrigerator', 'Washing Machine', 'Dryer'];

    this.applianceOptions = {
      brand: this.applianceBrands,
      category: this.categories,
      selection: this.applianceSelections
    };
    this.applianceSelected = {
      brand: this.applianceBrands[0],
      selection: this.applianceSelections[0],
      category: this.categories[0]
    };

    // Bikes
    this.bikeBrands = ['Giant', 'Trek', 'Specialized', 'Cannondale', 'Santa Cruz', 'GT', 'Scott'];
    this.bikeSelections = ['bike', 'frame', 'wheels', 'derailleur', 'helmet'];

    this.bikeOptions = {brand: this.bikeBrands, category: this.categories, selection: this.bikeSelections};
    this.bikeSelected = {
      brand: this.bikeBrands[0],
      selection: this.bikeSelections[0],
      category: this.categories[0]
    };

    // Maps
    this.categoryMap.audio = {options: this.audioOptions, selected: this.audioSelected};
    this.categoryMap.appliance = {options: this.applianceOptions, selected: this.applianceSelected};
    this.categoryMap.bikes = {options: this.bikeOptions, selected: this.bikeSelected};

    // Default Mappings
    this.options = this.categoryMap.audio.options;
    this.selected = this.categoryMap.audio.selected;


    this.changeSubscribers = {};
  }

  subscribe(name, cb) {
    this.changeSubscribers[name] || (this.changeSubscribers[name] = []);
    this.changeSubscribers[name].push(cb);
  }

  notify(name) {
    if (!this.changeSubscribers[name]) {
      return;
    }

    // if you want canceling or anything else, add it in to this cb loop
    this.changeSubscribers[name].forEach(function (cb) {
      cb();
    });
  }

  getSelected(selectType) {
    return this.selected[selectType];
  }

  setSelectedAndNotify(selectType, value) {
    if (selectType === 'category') {
      this.selected[selectType] = value;
      this.options = this.categoryMap[value].options;
      this.selected = this.categoryMap[value].selected;
      this.notify('categoryChange');
    } else {
      this.selected[selectType] = value;
    }
  }

  getOptions(categoryType) {
    return this.options[categoryType];
  }

  getTitle() {
    return this.getSelected('selection') + '  ' + this.getSelected('brand');
  }
}

export default StateObject;

